package com.example.todoapp

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build.ID


class SQLiteHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    // below is the method for creating a database by a sqlite query
    override fun onCreate(db: SQLiteDatabase) {
        // below is a sqlite query, where column names
        // along with their data types is given
        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY, " +
                INFO_COl + " TEXT" + ")")

        // we are calling sqlite
        // method for executing our query
        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        // this method is to check if table already exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    // This method is for adding data in our database
    fun addName(info : String){

        // below we are creating
        // a content values variable
        print("-1----------------------------------------------------------")
        val values = ContentValues()
        print("-2----------------------------------------------------------")
        // we are inserting our values
        // in the form of key-value pair
        values.put(INFO_COl, info)
        //values.put(AGE_COL, id)

        // here we are creating a
        // writable variable of
        // our database as we want to
        // insert value in our database
        val db = this.writableDatabase

        // all values are inserted into database
        db.insert(TABLE_NAME, null, values)

        // at last we are
        // closing our database
        db.close()
    }
/*
    fun update(info : String, id: String)
    {
        val selectQuery = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(selectQuery, null)
        val db = this.readableDatabase
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
    }
*/
    fun update(info : String, id: String): Boolean{
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(INFO_COl, info)
        }
        val _success = db.update(TABLE_NAME, values, ID_COL + "=?", arrayOf(id)).toLong()
        db.close()
        return ("$_success").toInt() != -1
    }

    fun delete(id: String): Boolean {
        val db = this.writableDatabase
        val _success = db.delete(TABLE_NAME, ID_COL + "=?", arrayOf(id)).toLong()
        return ("$_success").toInt() != -1
    }

    fun deleteAll(): Boolean {
        val db = this.writableDatabase
        val _success = db.delete(TABLE_NAME, null,null).toLong()
        db.close()
        return ("$_success").toInt() != -1
    }

    // below method is to get
    // all data from our database
    fun getName(): Cursor? {

        // here we are creating a readable
        // variable of our database
        // as we want to read value from it
        val db = this.readableDatabase

        // below code returns a cursor to
        // read data from the database
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null)

    }

    companion object{
        // here we have defined variables for our database

        // below is variable for database name
        private val DATABASE_NAME = "TAREFAS"

        // below is the variable for database version
        private val DATABASE_VERSION = 1

        // below is the variable for table name
        val TABLE_NAME = "tarefas_table"

        // below is the variable for id column
        val ID_COL = "id"

        // below is the variable for name column
        val INFO_COl = "info"
    }
}