package com.example.todoapp

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView




class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    class Tarefa(tipo: Boolean, texto: String, status: Boolean, posi: Int) {
        var tipo: Boolean = tipo
        var texto: String = texto
        var status: Boolean = status
        var posi: Int = posi


        fun getObjeto(): String {
            if (tipo == false) {
                val concat = "(NÃO É URGENTE) " + texto;
                return concat
            } else {
                val concat = "(É URGENTE) " + texto;
                return concat
            }
        }
    }

    var size: Int = 0

    val listaTarefas = mutableListOf<Tarefa>()
    val listaTarefasFeitas = mutableListOf<Tarefa>()

    fun a(v: View)
    {
        // getting the recyclerview by its id
        val recyclerview = findViewById<RecyclerView>(R.id.recyclerview)

        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(this)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
/*
        for (i in 1..20) {
            data.add(ItemsViewModel( "Item " + i))
        }*/
        val db = SQLiteHelper(this, null)

        val cursor = db.getName()
        var info = String()
        var id = String()
        if(cursor != null){
            if(cursor.moveToFirst()){
                do
                {
                    //var info = String()
                    info = cursor.getString(cursor.getColumnIndexOrThrow(SQLiteHelper.INFO_COl))
                    id = cursor.getString(cursor.getColumnIndexOrThrow(SQLiteHelper.ID_COL))
                    data.add(ItemsViewModel("(" + id + ") " + info))
                }while(cursor.moveToNext())
            }
        }


       /*
        listaTarefas.forEach { i ->
            data.add(ItemsViewModel(i.getObjeto()))
        }*/

        print("-7----------------------------------------------------------")



        // This will pass the ArrayList to our Adapter
        val adapter = CustomAdapter(data)

        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter




        //====================================================================




        // calling method to add
        // name to our database


        // Toast to message on the screen
        //Toast.makeText(this, name + " added to database", Toast.LENGTH_LONG).show()




    }


    fun updateTo(v: View)
    {
        val valorInput = findViewById<EditText>(R.id.editTextTextPersonName).text.toString();
        val valorId = findViewById<EditText>(R.id.editTextTextPersonName2).text.toString();
        val valorUrgente = findViewById<Switch>(R.id.switch1).isChecked;

        val db = SQLiteHelper(this, null)

        val objetoGenerico = Tarefa(valorUrgente, valorInput, false, size)
        db.update(objetoGenerico.getObjeto(),valorId)

        a(v)
    }

    fun deleteTo(v: View)
    {
        val valorId = findViewById<EditText>(R.id.editTextTextPersonName2).text.toString();

        val db = SQLiteHelper(this, null)
        db.delete(valorId)
        a(v)
    }

    fun deleteAllTo(v: View)
    {
        val db = SQLiteHelper(this, null)
        db.deleteAll()
        a(v)
    }

    fun addTo(v: View)
    {
        //findViewById<LinearLayout>(R.id.vertical).removeAllViews()
        val valorInput = findViewById<EditText>(R.id.editTextTextPersonName).text.toString();
        val valorUrgente = findViewById<Switch>(R.id.switch1).isChecked;

        val objetoGenerico = Tarefa(valorUrgente, valorInput, false, size)


        //val listaTarefas = mutableListOf<Tarefa>()
        listaTarefas.add(objetoGenerico)

        listaTarefas.forEach { i ->
            println("$i")

            val card = LinearLayout(this)

            val texto = TextView(this)

            val checkboxi = CheckBox(this)

            texto.text = "${i.getObjeto()}"

            checkboxi.isChecked = i.status
            checkboxi.text = "Feito"

            checkboxi.setOnClickListener {
                if (checkboxi.isChecked) {
                    //findViewById<LinearLayout>(R.id.vertical).removeAllViews()
                    listaTarefasFeitas.add(i)


                    listaTarefasFeitas.forEach { i ->
                        val card = LinearLayout(this)

                        val texto = TextView(this)

                        val checkboxi = CheckBox(this)

                        texto.text = "${i.getObjeto()}"

                        checkboxi.isChecked = i.status
                        checkboxi.text = "Feito"

                        checkboxi.id = i.posi

                        card.addView(texto)
                        card.addView(checkboxi)

                        //findViewById<LinearLayout>(R.id.vertical).addView(card)
                    }
                } else {
                    //findViewById<LinearLayout>(R.id.vertical).removeAllViews()
                    listaTarefasFeitas.remove(i)

                    listaTarefas.forEach { i ->
                        //findViewById<LinearLayout>(R.id.vertical).addView(card)
                    }
                }
            }

            card.addView(texto)
            card.addView(checkboxi)

            //findViewById<LinearLayout>(R.id.vertical).addView(card)
        }
        size++

        print("-8----------------------------------------------------------")
        val db = SQLiteHelper(this, null)
        print("-4----------------------------------------------------------")

        db.addName(listaTarefas.get(listaTarefas.size - 1).getObjeto())
        a(v)
    }

    fun update(v: View)
    {
        if (findViewById<CheckBox>(R.id.onlyDone).isChecked == true)
        {
            //findViewById<LinearLayout>(R.id.vertical).removeAllViews()
            listaTarefasFeitas.forEach { i ->
                val card = LinearLayout(this)

                val texto = TextView(this)

                val checkboxi = CheckBox(this)

                texto.text = "${i.getObjeto()}"

                checkboxi.isChecked = i.status
                checkboxi.text = "Feito"

                checkboxi.id = i.posi

                card.addView(texto)
                card.addView(checkboxi)

                //findViewById<LinearLayout>(R.id.vertical).addView(card)
            }
        } else {
            //findViewById<LinearLayout>(R.id.vertical).removeAllViews()
            listaTarefas.forEach { i ->
                val card = LinearLayout(this)

                val texto = TextView(this)

                val checkboxi = CheckBox(this)

                texto.text = "${i.getObjeto()}"

                checkboxi.isChecked = i.status
                checkboxi.text = "Feito"

                checkboxi.id = i.posi

                card.addView(texto)
                card.addView(checkboxi)

                //findViewById<LinearLayout>(R.id.vertical).addView(card)
            }
        }
    }
}